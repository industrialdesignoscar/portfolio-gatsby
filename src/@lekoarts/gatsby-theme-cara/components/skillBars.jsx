import SkillBar from 'react-skillbars';

const skills = [
  {type: "Java", level: 85},
  {type: "Javascript", level: 75},
];

<SkillBar skills={skills}/>