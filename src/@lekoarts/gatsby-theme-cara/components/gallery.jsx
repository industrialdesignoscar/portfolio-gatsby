import * as React from'react';
import Gallery from 'react-photo-gallery';

const photos = [
  {
    src: "https://firebasestorage.googleapis.com/v0/b/test-gatsby-portfolio.appspot.com/o/portfolio-images%2Fmodeling-rendering%2Fproducts-design%2F1_PERSPECTIVA_ARTICULADA.JPG?alt=media&token=cd1e1236-f077-4340-b977-faabc20b1805",
    width: 4,
    height: 3
  },
  {
    src: "https://firebasestorage.googleapis.com/v0/b/test-gatsby-portfolio.appspot.com/o/portfolio-images%2Fmodeling-rendering%2Fproducts-design%2F1-Perspectiva.jpg?alt=media&token=310f5906-e585-417f-a3d8-9a8086a3b3b0",
    width: 4,
    height: 3
  },
  {
    src: "https://firebasestorage.googleapis.com/v0/b/test-gatsby-portfolio.appspot.com/o/portfolio-images%2Fmodeling-rendering%2Fproducts-design%2F1-Perspectiva.jpg?alt=media&token=310f5906-e585-417f-a3d8-9a8086a3b3b0",
    width: 4,
    height: 3
  },
  {
    src: "https://firebasestorage.googleapis.com/v0/b/test-gatsby-portfolio.appspot.com/o/portfolio-images%2Fmodeling-rendering%2Fproducts-design%2F1-Perspectiva.jpg?alt=media&token=310f5906-e585-417f-a3d8-9a8086a3b3b0",
    width: 4,
    height: 3
  }
]

function AboutGallery(){
    return (
        <Gallery photos = {photos}/>
    )
}

export default AboutGallery;